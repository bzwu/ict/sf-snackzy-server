-- MySQL dump 10.16  Distrib 10.3.7-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db_snackzy
-- ------------------------------------------------------
-- Server version	10.3.7-MariaDB-1:10.3.7+maria~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:uuid)',
  `last_answered_question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C74404559A99254C` (`last_answered_question_id`),
  CONSTRAINT `FK_C74404559A99254C` FOREIGN KEY (`last_answered_question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES ('1d7bfdbd-79b2-4fa7-91ac-34e0ec049eca',1),('9594561e-a6b8-461c-b0ae-cad01f847f97',4);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_question`
--

DROP TABLE IF EXISTS `client_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_question` (
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:uuid)',
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`,`question_id`),
  KEY `IDX_9811798B19EB6921` (`client_id`),
  KEY `IDX_9811798B1E27F6BF` (`question_id`),
  CONSTRAINT `FK_9811798B19EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_9811798B1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_question`
--

LOCK TABLES `client_question` WRITE;
/*!40000 ALTER TABLE `client_question` DISABLE KEYS */;
INSERT INTO `client_question` VALUES ('9594561e-a6b8-461c-b0ae-cad01f847f97',3);
/*!40000 ALTER TABLE `client_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client_wrong_question`
--

DROP TABLE IF EXISTS `client_wrong_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_wrong_question` (
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:uuid)',
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`,`question_id`),
  KEY `IDX_B013A2CC19EB6921` (`client_id`),
  KEY `IDX_B013A2CC1E27F6BF` (`question_id`),
  CONSTRAINT `FK_B013A2CC19EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B013A2CC1E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_wrong_question`
--

LOCK TABLES `client_wrong_question` WRITE;
/*!40000 ALTER TABLE `client_wrong_question` DISABLE KEYS */;
INSERT INTO `client_wrong_question` VALUES ('9594561e-a6b8-461c-b0ae-cad01f847f97',1),('9594561e-a6b8-461c-b0ae-cad01f847f97',2),('9594561e-a6b8-461c-b0ae-cad01f847f97',4);
/*!40000 ALTER TABLE `client_wrong_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20181122181901'),('20181122185737'),('20181122185924'),('20181122190324'),('20181123084546'),('20181123092634');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `next_question_id` int(11) DEFAULT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answers` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B6F7494E1CF5F25E` (`next_question_id`),
  CONSTRAINT `FK_B6F7494E1CF5F25E` FOREIGN KEY (`next_question_id`) REFERENCES `question` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,2,'Wie viele Mitarbeiter hat die Firma Snackzy?','{\"83\":true,\"24\":false,\"138\":false}'),(2,3,'Seit wann besteht die Firma Snackzy?','{\"2001\":true,\"1996\":false,\"1975\":false}'),(3,4,'Was ist das spezielle an unserer Schokolade?','{\"Die Zubereitungsart\":true,\"Die Zutaten\":false,\"Das Herstellungsklima\":false}'),(4,NULL,'Wie viele verschiedene Popcornsorten verkaufen wir?','{\"12\":true,\"9\":false,\"7\":false}');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18 15:21:02
