<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     *
     * @Assert\Uuid
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Question", inversedBy="answeredBy")
     */
    private $correctQuestions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="lastAnsweredBy")
     */
    private $lastAnsweredQuestion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Question", inversedBy="wrongClients")
     * @ORM\JoinTable(name="client_wrong_question")
     */
    private $wrongQuestions;

    public function __construct()
    {
        $this->correctQuestions = new ArrayCollection();
        $this->wrongQuestions = new ArrayCollection();
    }

    public function getNextQuestion(): ?Question
    {
        // Returns null if no question has been answered
        if (null === $this->getLastAnsweredQuestion()) {
            return null;
        }

        // Throw exception if there are no more questions left to answer
        if (!$this->getLastAnsweredQuestion()->hasNext()) {
            throw new \RuntimeException('There are no more questions to answer');
        }

        return $this->getLastAnsweredQuestion()->getNextQuestion();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    /**
     * @return Collection|Question[]
     */
    public function getCorrectQuestions(): Collection
    {
        return $this->correctQuestions;
    }

    public function addCorrectQuestion(Question $answeredQuestion): self
    {
        if (!$this->correctQuestions->contains($answeredQuestion)) {
            $this->correctQuestions[] = $answeredQuestion;
            $this->setLastAnsweredQuestion($answeredQuestion);
        }

        return $this;
    }

    public function removeCorrectQuestion(Question $answeredQuestion): self
    {
        if ($this->correctQuestions->contains($answeredQuestion)) {
            $this->correctQuestions->removeElement($answeredQuestion);
        }

        return $this;
    }

    public function getLastAnsweredQuestion(): ?Question
    {
        return $this->lastAnsweredQuestion;
    }

    public function setLastAnsweredQuestion(?Question $lastAnsweredQuestion): self
    {
        $this->lastAnsweredQuestion = $lastAnsweredQuestion;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getWrongQuestions(): Collection
    {
        return $this->wrongQuestions;
    }

    public function addWrongQuestion(Question $wrongQuestion): self
    {
        if (!$this->wrongQuestions->contains($wrongQuestion)) {
            $this->wrongQuestions[] = $wrongQuestion;
            $this->setLastAnsweredQuestion($wrongQuestion);
        }

        return $this;
    }

    public function removeWrongQuestion(Question $wrongQuestion): self
    {
        if ($this->wrongQuestions->contains($wrongQuestion)) {
            $this->wrongQuestions->removeElement($wrongQuestion);
        }

        return $this;
    }
}
