<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $question;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Question", inversedBy="lastQuestion", cascade={"persist", "remove"})
     *
     * @var Question|null
     */
    private $nextQuestion;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Question", mappedBy="nextQuestion", cascade={"persist", "remove"})
     *
     * @var Question|null
     */
    private $lastQuestion;

    /**
     * @ORM\Column(type="json_array")
     *
     * @var array
     */
    private $answers;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Client", mappedBy="answeredQuestions")
     *
     * @var Client[]|ArrayCollection|null
     */
    private $answeredBy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="lastAnsweredQuestion")
     *
     * @var Client[]|ArrayCollection|null
     */
    private $lastAnsweredBy;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Client", mappedBy="wrongQuestions")
     */
    private $wrongClients;

    public function __construct()
    {
        $this->answeredBy = new ArrayCollection();
        $this->lastAnsweredBy = new ArrayCollection();
        $this->wrongClients = new ArrayCollection();
    }

    public static function sanitize(string $question): string
    {
        return strtolower(trim($question));
    }

    public function getCorrectAnswer(bool $lowerCase = true): string
    {
        /**
         * @var string $answer
         * @var bool $isCorrect
         */
        foreach ($this->getAnswers() as $answer => $isCorrect) {
            if ($isCorrect === true) {
                return $lowerCase ? self::sanitize($answer) : $answer;
            }
        }

        return '';
    }

    public function hasNext(): bool
    {
        return $this->getNextQuestion() instanceof self;
    }

    public function __toString(): string
    {
        return (string)$this->getQuestion();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getNextQuestion(): ?self
    {
        return $this->nextQuestion;
    }

    public function setNextQuestion(?self $nextQuestion): self
    {
        $this->nextQuestion = $nextQuestion;

        return $this;
    }

    public function getLastQuestion(): ?self
    {
        return $this->lastQuestion;
    }

    public function setLastQuestion(?self $lastQuestion): self
    {
        $this->lastQuestion = $lastQuestion;

        // set (or unset) the owning side of the relation if necessary
        $newNextQuestion = $lastQuestion === null ? null : $this;
        if ($newNextQuestion !== $lastQuestion->getNextQuestion()) {
            $lastQuestion->setNextQuestion($newNextQuestion);
        }

        return $this;
    }

    public function getAnswers(): ?array
    {
        return $this->answers;
    }

    public function setAnswers($answers): self
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getAnsweredBy(): Collection
    {
        return $this->answeredBy;
    }

    public function addAnsweredBy(Client $answeredBy): self
    {
        if (!$this->answeredBy->contains($answeredBy)) {
            $this->answeredBy[] = $answeredBy;
            $answeredBy->addCorrectQuestion($this);
        }

        return $this;
    }

    public function removeAnsweredBy(Client $answeredBy): self
    {
        if ($this->answeredBy->contains($answeredBy)) {
            $this->answeredBy->removeElement($answeredBy);
            $answeredBy->removeCorrectQuestion($this);
        }

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getLastAnsweredBy(): Collection
    {
        return $this->lastAnsweredBy;
    }

    public function addLastAnsweredBy(Client $lastAnsweredBy): self
    {
        if (!$this->lastAnsweredBy->contains($lastAnsweredBy)) {
            $this->lastAnsweredBy[] = $lastAnsweredBy;
            $lastAnsweredBy->setLastAnsweredQuestion($this);
        }

        return $this;
    }

    public function removeLastAnsweredBy(Client $lastAnsweredBy): self
    {
        if ($this->lastAnsweredBy->contains($lastAnsweredBy)) {
            $this->lastAnsweredBy->removeElement($lastAnsweredBy);
            // set the owning side to null (unless already changed)
            if ($lastAnsweredBy->getLastAnsweredQuestion() === $this) {
                $lastAnsweredBy->setLastAnsweredQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Client[]
     */
    public function getWrongClients(): Collection
    {
        return $this->wrongClients;
    }

    public function addWrongClient(Client $wrongClient): self
    {
        if (!$this->wrongClients->contains($wrongClient)) {
            $this->wrongClients[] = $wrongClient;
            $wrongClient->addWrongQuestion($this);
        }

        return $this;
    }

    public function removeWrongClient(Client $wrongClient): self
    {
        if ($this->wrongClients->contains($wrongClient)) {
            $this->wrongClients->removeElement($wrongClient);
            $wrongClient->removeWrongQuestion($this);
        }

        return $this;
    }
}
