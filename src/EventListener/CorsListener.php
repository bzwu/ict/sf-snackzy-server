<?php
declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class CorsListener
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\EventListener
 */
class CorsListener
{
    public function onKernelResponse(FilterResponseEvent $event): void
    {
        $responseHeaders = $event->getResponse()->headers;

        $responseHeaders->set('Access-Control-Allow-Origin', '*');
        $responseHeaders->set('Access-Control-Allow-Methods', '*');
        $responseHeaders->set('Access-Control-Allow-Headers', '*');
    }
}
