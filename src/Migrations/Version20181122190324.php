<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181122190324 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE client_question (client_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', question_id INT NOT NULL, INDEX IDX_9811798B19EB6921 (client_id), INDEX IDX_9811798B1E27F6BF (question_id), PRIMARY KEY(client_id, question_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_question ADD CONSTRAINT FK_9811798B19EB6921 FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client_question ADD CONSTRAINT FK_9811798B1E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE client ADD last_answered_question_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C74404559A99254C FOREIGN KEY (last_answered_question_id) REFERENCES question (id)');
        $this->addSql('CREATE INDEX IDX_C74404559A99254C ON client (last_answered_question_id)');
        $this->addSql('ALTER TABLE question CHANGE next_question_id next_question_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE client_question');
        $this->addSql('ALTER TABLE client DROP FOREIGN KEY FK_C74404559A99254C');
        $this->addSql('DROP INDEX IDX_C74404559A99254C ON client');
        $this->addSql('ALTER TABLE client DROP last_answered_question_id');
        $this->addSql('ALTER TABLE question CHANGE next_question_id next_question_id INT DEFAULT NULL');
    }
}
