<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Question;
use App\Repository\QuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class QuestionController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 *
 * @Route("/question", name="question.")
 */
class QuestionController extends AbstractApiController
{
    /**
     * @Route(
     *     "/get-next/{clientId}",
     *     name="get_next",
     *     methods={"GET"},
     *     requirements={
     *         "clientId"="^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"
     *     })
     *
     * @param string              $clientId
     *
     * @return JsonResponse
     */
    public function getNext(string $clientId): JsonResponse
    {
        try {
            $em = $this->getDoctrine()->getManager();
            /** @var Client $client */
            $client = $em->getRepository(Client::class)->find($clientId);
            /** @var QuestionRepository $questionRepo */
            $questionRepo = $em->getRepository(Question::class);
            $question = $client->getNextQuestion() ?? $questionRepo->findFirst(); // ->findFirst resolves entry-question if client has not answered a question so far
        } catch (\Exception $e) {
            return $this->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        if (null === $question) {
            return $this->json([
                'success' => false,
                'message' => $this->translator->trans('question.get_next.resolved_null')
            ]);
        }

        $answers = array_keys($question->getAnswers());
        shuffle($answers);

        return $this->json([
            'success' => true,
            'question' => [
                'id' => $question->getId(),
                'question' => $question->getQuestion(),
                'answers' => $answers,
                'nextQuestionId' => $question->getNextQuestion() ? $question->getNextQuestion()->getId() : null
            ]
        ]);
    }

    /**
     * @Route("/get-wrong/{clientId}",
     *     name="get_wrong",
     *     methods={"GET"})
     *
     * @param string $clientId
     *
     * @return JsonResponse
     */
    public function getWrong(string $clientId): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $clientRepo = $em->getRepository(Client::class);
        $wrongQuestions = [];

        try {
            /** @var Client $client */
            $client = $clientRepo->findOneByUuid($clientId);
        } catch (\Exception $e) {
            return $this->failure($this->translator->trans('question.get_all.error_while_resolve_questions'));
        }

        if (null === $client) {
            return $this->failure($this->translator->trans('question.answer.undefined_client'));
        }

        foreach ($client->getWrongQuestions() as $wrongQuestion) {
            $wrongQuestions[] = [
                'id' => $wrongQuestion->getId(),
                'question' => $wrongQuestion->getQuestion(),
                'correctAnswer' => $wrongQuestion->getCorrectAnswer(false)
            ];
        }

        return $this->json([
            'success' => true,
            'wrongQuestions' => $wrongQuestions
        ]);
    }

    /**
     * @Route(
     *     "/get-all/{clientId}",
     *     name="get_all",
     *     methods={"GET"})
     *
     * @param string $clientId
     *
     * @return JsonResponse
     */
    public function getAll(string $clientId): JsonResponse
    {
        $unansweredQuestions = [];

        try {
            $em = $this->getDoctrine()->getManager();
            /** @var Client $client */
            $client = $em->getRepository(Client::class)->find($clientId);
            /** @var Question[]|ArrayCollection $questions */
            $questions = $em->getRepository(Question::class)->findAll();

            if (count($questions) <= 0) {
                return $this->failure($this->translator->trans('question.get_all.no_questions'));
            }

            foreach ($questions as $question) {
                if ($client->getCorrectQuestions()->contains($question)) {
                    continue;
                }

                $answers = array_keys($question->getAnswers());
                shuffle($answers);

                $unansweredQuestions[] = [
                    'id' => $question->getId(),
                    'question' => $question->getQuestion(),
                    'answers' => $answers,
                    'nextQuestionId' => $question->getNextQuestion() ? $question->getNextQuestion()->getId() : null
                ];
            }
        } catch (\Exception $e) {
            return $this->failure($this->translator->trans('question.get_all.error_while_resolve_questions'));
        }

        if (count($unansweredQuestions) <= 0) {
            return $this->failure($this->translator->trans('question.get_all.all_answered'));
        }

        return $this->json([
            'success' => true,
            'questions' => $unansweredQuestions
        ]);
    }

    /**
     * @Route(
     *     "/list",
     *     name="list",
     *     methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $questions = $this->getDoctrine()->getRepository(Question::class)->findAll();
        $processedData = [];

        /** @var Question $question */
        foreach ($questions as $question) {
            $processedData[] = [
                'id' => $question->getId(),
                'question' => $question->getQuestion(),
                'answers' => array_keys($question->getAnswers()),
                'next_question' => $question->getNextQuestion() ? $question->getNextQuestion()->getId() : null,
                'last_question' => $question->getLastQuestion() ? $question->getLastQuestion()->getId(): null
            ];
        }

        return $this->json($processedData);
    }

    /**
     * @Route(
     *     "/{questionId<\d+>}/answer/{answer}",
     *     name="answer",
     *     methods={"POST","OPTIONS"})
     *
     * @param Request  $request
     * @param int $questionId
     * @param string   $answer
     *
     * @return JsonResponse
     */
    public function answer(Request $request, int $questionId, string $answer): JsonResponse
    {
        if ($request->isMethod(Request::METHOD_OPTIONS)) {
            return new JsonResponse();
        }

        //return $this->json(['test']);
        $clientUuid = $request->request->get('uuid');
        $em = $this->getDoctrine()->getManager();
        /** @var Question $question */
        $question = $em->getRepository(Question::class)->find($questionId);
        /** @var Client $client */
        $client = $em->getRepository(Client::class)->findOneByUuid($clientUuid);
        $nextQuestion = false;

        if (null === $question) {
            throw $this->createNotFoundException($this->translator->trans('question.answer.undefined_question'));
        }

        if (null === $client) {
            return $this->failure($this->translator->trans('question.answer.undefined_client'));
        }

        if ($question->hasNext() === true && null !== $next = $question->getNextQuestion()) {
            $nextQuestion = $next->getId();
        }

        if ($question->getCorrectAnswer() === Question::sanitize($answer)) {
            $client->addCorrectQuestion($question);

            // submit to DB
            $em->persist($client);
            $em->flush();

            return $this->json([
                'success' => true
            ]);
        }

        // Add wrong question and submit to DB
        $client->addWrongQuestion($question);
        $em->persist($client);
        $em->flush();

        return $this->json([
            'success' => false
        ]);
    }
}
