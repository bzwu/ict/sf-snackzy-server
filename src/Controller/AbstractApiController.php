<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractApiController
 *
 * @author Scrummer <scrummer@gmx.ch>
 * @package App\Controller
 */
abstract class AbstractApiController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * AbstractApiController constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    protected function failure(string $message = '', array $additionalParams = [], int $status = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return $this->json(array_merge([
            'success' => false,
            'message' => $message
        ], $additionalParams), $status);
    }
}
