<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 *
 * @Route(name="default.")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Snackzy API'
        ]);
    }
}
