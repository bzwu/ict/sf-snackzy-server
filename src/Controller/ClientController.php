<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ClientController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 *
 * @Route("/client", name="client.")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/list", name="list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $clients = $this->getDoctrine()->getRepository(Client::class)->findAll();

        return $this->json(array_map(function (Client $client) {
            return [
                'id' => $client->getId(),
                'answeredQuestions' => ''
            ];
        }, $clients));
    }

    /**
     * Create a new client
     *
     * @Route("/create", name="create", methods={"GET"})
     *
     * @return JsonResponse Success-message & client on success, failure message otherwise
     */
    public function create(): JsonResponse
    {
        try {
            $em     = $this->getDoctrine()->getManager();
            $client = new Client();

            $em->persist($client);
            $em->flush();
        } catch (\Exception $e) {
            return $this->json(['success' => false, 'message' => $e->getMessage()]);
        }

        return $this->json([
            'success' => true,
            'client'  => $client->getId()
        ]);
    }
}
